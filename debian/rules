#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE=1

include /usr/share/ocaml/ocamlvars.mk

FRAMA_DIR=$(CURDIR)/debian/tmp
FRAMA_PLUGINS_DIR=$(CURDIR)/debian/frama-c-base/usr/lib/frama-c/plugins/

override_dh_auto_configure:
	mkdir -p lib/plugins
	touch .depend .make-clean .make-clean-stamp
	[ ! -f src/kernel/config.ml ] || cp src/kernel/config.ml src/kernel/config.ml.debian
	autoconf
	./configure $(CROSS) --prefix=/usr       \
		--mandir=\$${prefix}/share/man   \
		--infodir=\$${prefix}/share/info \
		--datarootdir=\$${prefix}/share  \
		--enable-verbosemake             \
		CFLAGS="$(CFLAGS)"               \
		LDFLAGS="-Wl,-z,defs"

FRAMA_PLUGINS=$(wildcard lib/plugins/*.cmx lib/plugins/*.cmxa)

override_dh_auto_install:
	$(MAKE) prefix=$(FRAMA_DIR)/usr FRAMAC_DATADIR=$(FRAMA_DIR)/usr/share/frama-c install
	# Fix permission of .c and .h files (they don't have to be executable)
	find debian/ -name "*.c" -or -name "*.h" -exec chmod a-x '{}' \;
	# On bytecode architectures, bin/frama-c.byte is same as bin/frama-c
ifeq ($(OCAML_OPT_ARCH),)
	$(RM) -f $(CURDIR)/debian/tmp/usr/bin/frama-c.byte
endif

override_dh_auto_clean:
	touch .depend
	[ ! -f src/kernel/config.ml.debian ] || mv src/kernel/config.ml.debian src/kernel/config.ml
	[ ! -f Makefile -o ! -f share/Makefile.config ] || $(MAKE) clean
	$(RM) -rf lib .depend

override_dh_ocaml:
	dh_ocaml --nodefined-map=frama-c-base:Big_int_Z,Z,Q

%:
	dh $@ --with ocaml --without autoreconf
