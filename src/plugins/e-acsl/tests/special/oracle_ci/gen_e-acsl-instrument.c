/* Generated by Frama-C */
#include "stdarg.h"
#include "stddef.h"
#include "stdio.h"
extern int __e_acsl_sound_verdict;

int __gen_e_acsl_uninstrument1(int *p);

int uninstrument1(int *p)
{
  int __retres;
  *p = 0;
  __retres = 0;
  return __retres;
}

/*@ requires \valid(p); */
int __gen_e_acsl_uninstrument2(int *p);

int uninstrument2(int *p)
{
  int __retres;
  {
    int *q = p;
    *p = 0;
    goto L;
  }
  L: __retres = 0;
  return __retres;
}

int instrument1(int *p)
{
  int __retres;
  __e_acsl_store_block((void *)(& p),(size_t)8);
  __e_acsl_initialize((void *)p,sizeof(int));
  *p = 0;
  __retres = 0;
  __e_acsl_delete_block((void *)(& p));
  return __retres;
}

/*@ requires \valid(p); */
int __gen_e_acsl_instrument2(int *p);

int instrument2(int *p)
{
  int __retres;
  __e_acsl_store_block((void *)(& p),(size_t)8);
  {
    int *q = p;
    __e_acsl_store_block((void *)(& q),(size_t)8);
    __e_acsl_full_init((void *)(& q));
    __e_acsl_initialize((void *)p,sizeof(int));
    *p = 0;
    __e_acsl_delete_block((void *)(& q));
    goto L;
    __e_acsl_delete_block((void *)(& q));
  }
  L: __retres = 0;
  __e_acsl_delete_block((void *)(& p));
  return __retres;
}

int vol(int n, void * const *__va_params)
{
  int __retres;
  va_list vl;
  int tmp;
  __e_acsl_store_block((void *)(& vl),(size_t)8);
  __e_acsl_store_block((void *)(& __va_params),(size_t)8);
  __e_acsl_full_init((void *)(& vl));
  vl = __va_params;
  tmp = *((int *)*vl);
  __e_acsl_full_init((void *)(& vl));
  vl ++;
  int r = tmp;
  __retres = 0;
  __e_acsl_delete_block((void *)(& __va_params));
  __e_acsl_delete_block((void *)(& vl));
  return __retres;
}

int main(void)
{
  int x;
  int tmp;
  __e_acsl_memory_init((int *)0,(char ***)0,(size_t)8);
  __e_acsl_store_block((void *)(& x),(size_t)4);
  int y = 0;
  __e_acsl_store_block((void *)(& y),(size_t)4);
  __e_acsl_full_init((void *)(& y));
  instrument1(& x);
  __gen_e_acsl_uninstrument1(& x);
  __gen_e_acsl_instrument2(& x);
  __gen_e_acsl_uninstrument2(& x);
  {
    int __gen_e_acsl_initialized;
    __gen_e_acsl_initialized = __e_acsl_initialized((void *)(& x),
                                                    sizeof(int));
    __e_acsl_assert(__gen_e_acsl_initialized,"Assertion","main",
                    "\\initialized(&x)","tests/special/e-acsl-instrument.c",
                    56);
  }
  /*@ assert \initialized(&x); */ ;
  {
    int __gen_e_acsl_initialized_2;
    __gen_e_acsl_initialized_2 = __e_acsl_initialized((void *)(& y),
                                                      sizeof(int));
    __e_acsl_assert(__gen_e_acsl_initialized_2,"Assertion","main",
                    "\\initialized(&y)","tests/special/e-acsl-instrument.c",
                    57);
  }
  /*@ assert \initialized(&y); */ ;
  {
    int __va_arg0 = 1;
    __e_acsl_store_block((void *)(& __va_arg0),(size_t)4);
    __e_acsl_full_init((void *)(& __va_arg0));
    void *__va_args[1] = {& __va_arg0};
    __e_acsl_store_block((void *)(__va_args),(size_t)8);
    __e_acsl_full_init((void *)(& __va_args));
    tmp = vol(6,(void * const *)(__va_args));
    __e_acsl_delete_block((void *)(& __va_arg0));
    __e_acsl_delete_block((void *)(__va_args));
  }
  __e_acsl_delete_block((void *)(& y));
  __e_acsl_delete_block((void *)(& x));
  __e_acsl_memory_clean();
  return tmp;
}

/*@ requires \valid(p); */
int __gen_e_acsl_instrument2(int *p)
{
  int __retres;
  {
    int __gen_e_acsl_valid;
    __e_acsl_store_block((void *)(& p),(size_t)8);
    __gen_e_acsl_valid = __e_acsl_valid((void *)p,sizeof(int),(void *)p,
                                        (void *)(& p));
    __e_acsl_assert(__gen_e_acsl_valid,"Precondition","instrument2",
                    "\\valid(p)","tests/special/e-acsl-instrument.c",29);
  }
  __retres = instrument2(p);
  __e_acsl_delete_block((void *)(& p));
  return __retres;
}

/*@ requires \valid(p); */
int __gen_e_acsl_uninstrument2(int *p)
{
  int __retres;
  {
    int __gen_e_acsl_valid;
    __gen_e_acsl_valid = __e_acsl_valid((void *)p,sizeof(int),(void *)p,
                                        (void *)(& p));
    __e_acsl_assert(__gen_e_acsl_valid,"Precondition","uninstrument2",
                    "\\valid(p)","tests/special/e-acsl-instrument.c",14);
  }
  __e_acsl_sound_verdict = 0;
  __retres = uninstrument2(p);
  return __retres;
}

int __gen_e_acsl_uninstrument1(int *p)
{
  int __retres;
  __e_acsl_sound_verdict = 0;
  __retres = uninstrument1(p);
  return __retres;
}


