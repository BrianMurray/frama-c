/* Generated by Frama-C */
#include "stddef.h"
#include "stdio.h"
extern int __e_acsl_sound_verdict;

int global_i;

int *global_i_ptr = & global_i;
int global_i = 0;
/*@ requires global_i ≡ 0;
    requires \valid(global_i_ptr);
    requires global_i_ptr ≡ &global_i;
 */
void __gen_e_acsl_loop(void);

void loop(void)
{
  return;
}

/*@ requires global_i ≡ 0;
    requires \valid(global_i_ptr);
    requires global_i_ptr ≡ &global_i;
 */
void __gen_e_acsl_loop(void)
{
  {
    int __gen_e_acsl_valid;
    __e_acsl_assert(global_i == 0,"Precondition","loop","global_i == 0",
                    "tests/bts/bts1478.c",9);
    __gen_e_acsl_valid = __e_acsl_valid((void *)global_i_ptr,sizeof(int),
                                        (void *)global_i_ptr,
                                        (void *)(& global_i_ptr));
    __e_acsl_assert(__gen_e_acsl_valid,"Precondition","loop",
                    "\\valid(global_i_ptr)","tests/bts/bts1478.c",10);
    __e_acsl_assert(global_i_ptr == & global_i,"Precondition","loop",
                    "global_i_ptr == &global_i","tests/bts/bts1478.c",11);
  }
  loop();
  return;
}

void __e_acsl_globals_init(void)
{
  static char __e_acsl_already_run = 0;
  if (! __e_acsl_already_run) {
    __e_acsl_already_run = 1;
    __e_acsl_store_block((void *)(& global_i_ptr),(size_t)8);
    __e_acsl_full_init((void *)(& global_i_ptr));
    __e_acsl_store_block((void *)(& global_i),(size_t)4);
    __e_acsl_full_init((void *)(& global_i));
  }
  return;
}

void __e_acsl_globals_clean(void)
{
  __e_acsl_delete_block((void *)(& global_i_ptr));
  __e_acsl_delete_block((void *)(& global_i));
}

int main(void)
{
  int __retres;
  __e_acsl_memory_init((int *)0,(char ***)0,(size_t)8);
  __e_acsl_globals_init();
  __gen_e_acsl_loop();
  __retres = 0;
  __e_acsl_globals_clean();
  __e_acsl_memory_clean();
  return __retres;
}


