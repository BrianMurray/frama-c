/* Generated by Frama-C */
#include "stddef.h"
#include "stdio.h"
extern int __e_acsl_sound_verdict;

/*@ requires \valid(Mtmax_in);
    requires \valid(Mwmax);
    requires \valid(Mtmax_out);
    
    behavior OverEstimate_Motoring:
      assumes \true;
      ensures
        *\old(Mtmax_out) ≢
        *\old(Mtmax_in) + (5 - ((5 / 80) * *\old(Mwmax)) * 0.4);
 */
void __gen_e_acsl_foo(float *Mtmax_in, float *Mwmax, float *Mtmax_out);

void foo(float *Mtmax_in, float *Mwmax, float *Mtmax_out)
{
  __e_acsl_store_block((void *)(& Mtmax_out),(size_t)8);
  __e_acsl_store_block((void *)(& Mwmax),(size_t)8);
  __e_acsl_store_block((void *)(& Mtmax_in),(size_t)8);
  __e_acsl_initialize((void *)Mtmax_out,sizeof(float));
  *Mtmax_out = (float)((double)*Mtmax_in + ((double)5 - (double)((float)(
                                                                 5 / 80) * *Mwmax) * 0.4));
  __e_acsl_delete_block((void *)(& Mtmax_out));
  __e_acsl_delete_block((void *)(& Mwmax));
  __e_acsl_delete_block((void *)(& Mtmax_in));
  return;
}

/*@ requires \valid(Mtmin_in);
    requires \valid(Mwmin);
    requires \valid(Mtmin_out);
    
    behavior UnderEstimate_Motoring:
      assumes \true;
      ensures
        *\old(Mtmin_out) ≡ *\old(Mtmin_in) < 0.85 * *\old(Mwmin)?
          *\old(Mtmin_in) ≢ 0.:
          0.85 * *\old(Mwmin) ≢ 0.;
 */
void __gen_e_acsl_bar(float *Mtmin_in, float *Mwmin, float *Mtmin_out);

void bar(float *Mtmin_in, float *Mwmin, float *Mtmin_out)
{
  __e_acsl_store_block((void *)(& Mtmin_out),(size_t)8);
  __e_acsl_store_block((void *)(& Mwmin),(size_t)8);
  __e_acsl_store_block((void *)(& Mtmin_in),(size_t)8);
  __e_acsl_initialize((void *)Mtmin_out,sizeof(float));
  *Mtmin_out = (float)(0.85 * (double)*Mwmin);
  __e_acsl_delete_block((void *)(& Mtmin_out));
  __e_acsl_delete_block((void *)(& Mwmin));
  __e_acsl_delete_block((void *)(& Mtmin_in));
  return;
}

int main(void)
{
  int __retres;
  float h;
  __e_acsl_memory_init((int *)0,(char ***)0,(size_t)8);
  __e_acsl_store_block((void *)(& h),(size_t)4);
  float f = (float)1.0;
  __e_acsl_store_block((void *)(& f),(size_t)4);
  __e_acsl_full_init((void *)(& f));
  float g = (float)1.0;
  __e_acsl_store_block((void *)(& g),(size_t)4);
  __e_acsl_full_init((void *)(& g));
  __gen_e_acsl_foo(& f,& g,& h);
  __gen_e_acsl_bar(& f,& g,& h);
  __retres = 0;
  __e_acsl_delete_block((void *)(& h));
  __e_acsl_delete_block((void *)(& g));
  __e_acsl_delete_block((void *)(& f));
  __e_acsl_memory_clean();
  return __retres;
}

/*@ requires \valid(Mtmin_in);
    requires \valid(Mwmin);
    requires \valid(Mtmin_out);
    
    behavior UnderEstimate_Motoring:
      assumes \true;
      ensures
        *\old(Mtmin_out) ≡ *\old(Mtmin_in) < 0.85 * *\old(Mwmin)?
          *\old(Mtmin_in) ≢ 0.:
          0.85 * *\old(Mwmin) ≢ 0.;
 */
void __gen_e_acsl_bar(float *Mtmin_in, float *Mwmin, float *Mtmin_out)
{
  float *__gen_e_acsl_at_6;
  float *__gen_e_acsl_at_5;
  float *__gen_e_acsl_at_4;
  float *__gen_e_acsl_at_3;
  float *__gen_e_acsl_at_2;
  float *__gen_e_acsl_at;
  __e_acsl_contract_t *__gen_e_acsl_contract;
  {
    int __gen_e_acsl_valid;
    int __gen_e_acsl_valid_2;
    int __gen_e_acsl_valid_3;
    __e_acsl_store_block((void *)(& Mtmin_out),(size_t)8);
    __e_acsl_store_block((void *)(& Mwmin),(size_t)8);
    __e_acsl_store_block((void *)(& Mtmin_in),(size_t)8);
    __gen_e_acsl_contract = __e_acsl_contract_init((size_t)1);
    __e_acsl_contract_set_behavior_assumes(__gen_e_acsl_contract,(size_t)0,1);
    __gen_e_acsl_valid = __e_acsl_valid((void *)Mtmin_in,sizeof(float),
                                        (void *)Mtmin_in,
                                        (void *)(& Mtmin_in));
    __e_acsl_assert(__gen_e_acsl_valid,"Precondition","bar",
                    "\\valid(Mtmin_in)","tests/bts/bts1307.i",20);
    __gen_e_acsl_valid_2 = __e_acsl_valid((void *)Mwmin,sizeof(float),
                                          (void *)Mwmin,(void *)(& Mwmin));
    __e_acsl_assert(__gen_e_acsl_valid_2,"Precondition","bar",
                    "\\valid(Mwmin)","tests/bts/bts1307.i",21);
    __gen_e_acsl_valid_3 = __e_acsl_valid((void *)Mtmin_out,sizeof(float),
                                          (void *)Mtmin_out,
                                          (void *)(& Mtmin_out));
    __e_acsl_assert(__gen_e_acsl_valid_3,"Precondition","bar",
                    "\\valid(Mtmin_out)","tests/bts/bts1307.i",22);
  }
  __gen_e_acsl_at_6 = Mwmin;
  __gen_e_acsl_at_5 = Mtmin_in;
  __gen_e_acsl_at_4 = Mwmin;
  __gen_e_acsl_at_3 = Mtmin_in;
  __gen_e_acsl_at_2 = Mtmin_in;
  __gen_e_acsl_at = Mtmin_out;
  bar(Mtmin_in,Mwmin,Mtmin_out);
  {
    int __gen_e_acsl_assumes_value;
    __gen_e_acsl_assumes_value = __e_acsl_contract_get_behavior_assumes
    ((__e_acsl_contract_t const *)__gen_e_acsl_contract,(size_t)0);
    if (__gen_e_acsl_assumes_value) {
      int __gen_e_acsl_valid_read;
      int __gen_e_acsl_valid_read_2;
      int __gen_e_acsl_and;
      int __gen_e_acsl_if;
      __gen_e_acsl_valid_read = __e_acsl_valid_read((void *)__gen_e_acsl_at_2,
                                                    sizeof(float),
                                                    (void *)__gen_e_acsl_at_2,
                                                    (void *)(& __gen_e_acsl_at_2));
      __e_acsl_assert(__gen_e_acsl_valid_read,"RTE","bar",
                      "mem_access: \\valid_read(__gen_e_acsl_at_2)",
                      "tests/bts/bts1307.i",26);
      __gen_e_acsl_valid_read_2 = __e_acsl_valid_read((void *)__gen_e_acsl_at,
                                                      sizeof(float),
                                                      (void *)__gen_e_acsl_at,
                                                      (void *)(& __gen_e_acsl_at));
      __e_acsl_assert(__gen_e_acsl_valid_read_2,"RTE","bar",
                      "mem_access: \\valid_read(__gen_e_acsl_at)",
                      "tests/bts/bts1307.i",26);
      if (*__gen_e_acsl_at == *__gen_e_acsl_at_2) {
        __e_acsl_mpq_t __gen_e_acsl_;
        __e_acsl_mpq_t __gen_e_acsl__2;
        __e_acsl_mpq_t __gen_e_acsl_mul;
        __e_acsl_mpq_t __gen_e_acsl__3;
        int __gen_e_acsl_lt;
        __gmpq_init(__gen_e_acsl_);
        __gmpq_set_str(__gen_e_acsl_,"085/100",10);
        __gmpq_init(__gen_e_acsl__2);
        __gmpq_set_d(__gen_e_acsl__2,(double)*__gen_e_acsl_at_4);
        __gmpq_init(__gen_e_acsl_mul);
        __gmpq_mul(__gen_e_acsl_mul,
                   (__e_acsl_mpq_struct const *)(__gen_e_acsl_),
                   (__e_acsl_mpq_struct const *)(__gen_e_acsl__2));
        __gmpq_init(__gen_e_acsl__3);
        __gmpq_set_d(__gen_e_acsl__3,(double)*__gen_e_acsl_at_3);
        __gen_e_acsl_lt = __gmpq_cmp((__e_acsl_mpq_struct const *)(__gen_e_acsl__3),
                                     (__e_acsl_mpq_struct const *)(__gen_e_acsl_mul));
        __gen_e_acsl_and = __gen_e_acsl_lt < 0;
        __gmpq_clear(__gen_e_acsl_);
        __gmpq_clear(__gen_e_acsl__2);
        __gmpq_clear(__gen_e_acsl_mul);
        __gmpq_clear(__gen_e_acsl__3);
      }
      else __gen_e_acsl_and = 0;
      if (__gen_e_acsl_and) {
        int __gen_e_acsl_valid_read_3;
        __gen_e_acsl_valid_read_3 = __e_acsl_valid_read((void *)__gen_e_acsl_at_5,
                                                        sizeof(float),
                                                        (void *)__gen_e_acsl_at_5,
                                                        (void *)(& __gen_e_acsl_at_5));
        __e_acsl_assert(__gen_e_acsl_valid_read_3,"RTE","bar",
                        "mem_access: \\valid_read(__gen_e_acsl_at_5)",
                        "tests/bts/bts1307.i",26);
        __gen_e_acsl_if = (double)*__gen_e_acsl_at_5 != 0.;
      }
      else {
        __e_acsl_mpq_t __gen_e_acsl__4;
        __e_acsl_mpq_t __gen_e_acsl__5;
        __e_acsl_mpq_t __gen_e_acsl_mul_2;
        __e_acsl_mpq_t __gen_e_acsl__6;
        int __gen_e_acsl_ne;
        __gmpq_init(__gen_e_acsl__4);
        __gmpq_set_str(__gen_e_acsl__4,"085/100",10);
        __gmpq_init(__gen_e_acsl__5);
        __gmpq_set_d(__gen_e_acsl__5,(double)*__gen_e_acsl_at_6);
        __gmpq_init(__gen_e_acsl_mul_2);
        __gmpq_mul(__gen_e_acsl_mul_2,
                   (__e_acsl_mpq_struct const *)(__gen_e_acsl__4),
                   (__e_acsl_mpq_struct const *)(__gen_e_acsl__5));
        __gmpq_init(__gen_e_acsl__6);
        __gmpq_set_d(__gen_e_acsl__6,0.);
        __gen_e_acsl_ne = __gmpq_cmp((__e_acsl_mpq_struct const *)(__gen_e_acsl_mul_2),
                                     (__e_acsl_mpq_struct const *)(__gen_e_acsl__6));
        __gen_e_acsl_if = __gen_e_acsl_ne != 0;
        __gmpq_clear(__gen_e_acsl__4);
        __gmpq_clear(__gen_e_acsl__5);
        __gmpq_clear(__gen_e_acsl_mul_2);
        __gmpq_clear(__gen_e_acsl__6);
      }
      __e_acsl_assert(__gen_e_acsl_if,"Postcondition","bar",
                      "UnderEstimate_Motoring:\n  *\\old(Mtmin_out) == *\\old(Mtmin_in) < 0.85 * *\\old(Mwmin)?\n    *\\old(Mtmin_in) != 0.:\n    0.85 * *\\old(Mwmin) != 0.",
                      "tests/bts/bts1307.i",26);
    }
    __e_acsl_contract_clean(__gen_e_acsl_contract);
    __e_acsl_delete_block((void *)(& Mtmin_out));
    __e_acsl_delete_block((void *)(& Mwmin));
    __e_acsl_delete_block((void *)(& Mtmin_in));
    return;
  }
}

/*@ requires \valid(Mtmax_in);
    requires \valid(Mwmax);
    requires \valid(Mtmax_out);
    
    behavior OverEstimate_Motoring:
      assumes \true;
      ensures
        *\old(Mtmax_out) ≢
        *\old(Mtmax_in) + (5 - ((5 / 80) * *\old(Mwmax)) * 0.4);
 */
void __gen_e_acsl_foo(float *Mtmax_in, float *Mwmax, float *Mtmax_out)
{
  float *__gen_e_acsl_at_3;
  float *__gen_e_acsl_at_2;
  float *__gen_e_acsl_at;
  __e_acsl_contract_t *__gen_e_acsl_contract;
  {
    int __gen_e_acsl_valid;
    int __gen_e_acsl_valid_2;
    int __gen_e_acsl_valid_3;
    __e_acsl_store_block((void *)(& Mtmax_out),(size_t)8);
    __e_acsl_store_block((void *)(& Mwmax),(size_t)8);
    __e_acsl_store_block((void *)(& Mtmax_in),(size_t)8);
    __gen_e_acsl_contract = __e_acsl_contract_init((size_t)1);
    __e_acsl_contract_set_behavior_assumes(__gen_e_acsl_contract,(size_t)0,1);
    __gen_e_acsl_valid = __e_acsl_valid((void *)Mtmax_in,sizeof(float),
                                        (void *)Mtmax_in,
                                        (void *)(& Mtmax_in));
    __e_acsl_assert(__gen_e_acsl_valid,"Precondition","foo",
                    "\\valid(Mtmax_in)","tests/bts/bts1307.i",5);
    __gen_e_acsl_valid_2 = __e_acsl_valid((void *)Mwmax,sizeof(float),
                                          (void *)Mwmax,(void *)(& Mwmax));
    __e_acsl_assert(__gen_e_acsl_valid_2,"Precondition","foo",
                    "\\valid(Mwmax)","tests/bts/bts1307.i",6);
    __gen_e_acsl_valid_3 = __e_acsl_valid((void *)Mtmax_out,sizeof(float),
                                          (void *)Mtmax_out,
                                          (void *)(& Mtmax_out));
    __e_acsl_assert(__gen_e_acsl_valid_3,"Precondition","foo",
                    "\\valid(Mtmax_out)","tests/bts/bts1307.i",7);
  }
  __gen_e_acsl_at_3 = Mwmax;
  __gen_e_acsl_at_2 = Mtmax_in;
  __gen_e_acsl_at = Mtmax_out;
  foo(Mtmax_in,Mwmax,Mtmax_out);
  {
    int __gen_e_acsl_assumes_value;
    __gen_e_acsl_assumes_value = __e_acsl_contract_get_behavior_assumes
    ((__e_acsl_contract_t const *)__gen_e_acsl_contract,(size_t)0);
    if (__gen_e_acsl_assumes_value) {
      __e_acsl_mpq_t __gen_e_acsl_;
      __e_acsl_mpq_t __gen_e_acsl__2;
      __e_acsl_mpq_t __gen_e_acsl__3;
      __e_acsl_mpq_t __gen_e_acsl_div;
      __e_acsl_mpq_t __gen_e_acsl__4;
      __e_acsl_mpq_t __gen_e_acsl_mul;
      __e_acsl_mpq_t __gen_e_acsl__5;
      __e_acsl_mpq_t __gen_e_acsl_mul_2;
      __e_acsl_mpq_t __gen_e_acsl_sub;
      __e_acsl_mpq_t __gen_e_acsl__6;
      __e_acsl_mpq_t __gen_e_acsl_add;
      __e_acsl_mpq_t __gen_e_acsl__7;
      int __gen_e_acsl_ne;
      __gmpq_init(__gen_e_acsl_);
      __gmpq_set_str(__gen_e_acsl_,"5",10);
      __gmpq_init(__gen_e_acsl__2);
      __gmpq_set_si(__gen_e_acsl__2,5L,1UL);
      __gmpq_init(__gen_e_acsl__3);
      __gmpq_set_si(__gen_e_acsl__3,80L,1UL);
      __gmpq_init(__gen_e_acsl_div);
      __gmpq_div(__gen_e_acsl_div,
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl__2),
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl__3));
      __gmpq_init(__gen_e_acsl__4);
      __gmpq_set_d(__gen_e_acsl__4,(double)*__gen_e_acsl_at_3);
      __gmpq_init(__gen_e_acsl_mul);
      __gmpq_mul(__gen_e_acsl_mul,
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl_div),
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl__4));
      __gmpq_init(__gen_e_acsl__5);
      __gmpq_set_str(__gen_e_acsl__5,"04/10",10);
      __gmpq_init(__gen_e_acsl_mul_2);
      __gmpq_mul(__gen_e_acsl_mul_2,
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl_mul),
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl__5));
      __gmpq_init(__gen_e_acsl_sub);
      __gmpq_sub(__gen_e_acsl_sub,
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl_),
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl_mul_2));
      __gmpq_init(__gen_e_acsl__6);
      __gmpq_set_d(__gen_e_acsl__6,(double)*__gen_e_acsl_at_2);
      __gmpq_init(__gen_e_acsl_add);
      __gmpq_add(__gen_e_acsl_add,
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl__6),
                 (__e_acsl_mpq_struct const *)(__gen_e_acsl_sub));
      __gmpq_init(__gen_e_acsl__7);
      __gmpq_set_d(__gen_e_acsl__7,(double)*__gen_e_acsl_at);
      __gen_e_acsl_ne = __gmpq_cmp((__e_acsl_mpq_struct const *)(__gen_e_acsl__7),
                                   (__e_acsl_mpq_struct const *)(__gen_e_acsl_add));
      __e_acsl_assert(__gen_e_acsl_ne != 0,"Postcondition","foo",
                      "OverEstimate_Motoring:\n  *\\old(Mtmax_out) != *\\old(Mtmax_in) + (5 - ((5 / 80) * *\\old(Mwmax)) * 0.4)",
                      "tests/bts/bts1307.i",11);
      __gmpq_clear(__gen_e_acsl_);
      __gmpq_clear(__gen_e_acsl__2);
      __gmpq_clear(__gen_e_acsl__3);
      __gmpq_clear(__gen_e_acsl_div);
      __gmpq_clear(__gen_e_acsl__4);
      __gmpq_clear(__gen_e_acsl_mul);
      __gmpq_clear(__gen_e_acsl__5);
      __gmpq_clear(__gen_e_acsl_mul_2);
      __gmpq_clear(__gen_e_acsl_sub);
      __gmpq_clear(__gen_e_acsl__6);
      __gmpq_clear(__gen_e_acsl_add);
      __gmpq_clear(__gen_e_acsl__7);
    }
    __e_acsl_contract_clean(__gen_e_acsl_contract);
    __e_acsl_delete_block((void *)(& Mtmax_out));
    __e_acsl_delete_block((void *)(& Mwmax));
    __e_acsl_delete_block((void *)(& Mtmax_in));
    return;
  }
}


