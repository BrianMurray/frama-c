/* Generated by Frama-C */
#include "stddef.h"
#include "stdio.h"
int f(void)
{
  int __retres;
  int a = 10;
  __e_acsl_store_block((void *)(& a),(size_t)4);
  __e_acsl_full_init((void *)(& a));
  goto lbl_1;
  lbl_2:
  /*@ assert \valid(&a); */
  {
    int __gen_e_acsl_valid;
    __gen_e_acsl_valid = __e_acsl_valid((void *)(& a),sizeof(int),
                                        (void *)(& a),(void *)0);
    __e_acsl_assert(__gen_e_acsl_valid,"Assertion","f","\\valid(&a)",
                    "tests/bts/issue-eacsl-105.c",11);
  }
  __retres = 0;
  goto return_label;
  lbl_1: goto lbl_2;
  return_label: {
                  __e_acsl_delete_block((void *)(& a));
                  return __retres;
                }
}

int main(void)
{
  int __retres;
  __e_acsl_memory_init((int *)0,(char ***)0,(size_t)8);
  f();
  __retres = 0;
  __e_acsl_memory_clean();
  return __retres;
}


